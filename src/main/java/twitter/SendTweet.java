package twitter;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("twitter")
public class SendTweet {

	@CrossOrigin
	@GetMapping(value="/sendTweet")
	public ResponseEntity<String> sendTweet() throws JsonProcessingException {

		TweetJson json = new TweetJson();

		json.setTitle("テスト");
		json.setText("テストの実施");

		ObjectMapper mapper = new ObjectMapper();
		String jsonStr = mapper.writeValueAsString(json);

		return ResponseEntity.ok().header("ContentType", MediaType.APPLICATION_JSON_VALUE.toString()).body(jsonStr);
	}
}