package twitter;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("twitter")
public class GetTweet {

	@CrossOrigin
	@PostMapping(value="/getTweet")
	public void getTweet(@RequestBody String json) {
		System.out.println(json);
	}
}